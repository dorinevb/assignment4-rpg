using Xunit;
using RPGCharacters;

namespace RPGCharactersTests
{
    public class CharacterTests
    {
        [Fact]
        public void Mage_MageLevelUponCreation_ShouldReturnOne()
        {
            // Arrange
            Mage mage1 = new Mage("MrMage");
            int expected = 1;
            // Act
            int actual = mage1.Level;
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Mage_MageLevelUponLevelUp_ShouldReturnTwo()
        {
            // Arrange
            Mage mage1 = new Mage("MrMage");
            int expected = 2;
            // Act
            mage1.LevelUp();
            int actual = mage1.Level;
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Mage_MageStatsUponCreation_ShouldReturnSTR1DEX1INT8()
        {
            // Arrange
            Mage mage1 = new Mage("MrMage");
            string expected = "STR=1, DEX=1, INT=8";
            // Act
            string actual = $"STR={mage1.Strength.CurrentAmount}, DEX={mage1.Dexterity.CurrentAmount}, INT={mage1.Intelligence.CurrentAmount}";
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Ranger_RangerStatsUponCreation_ShouldReturnSTR1DEX7INT1()
        {
            // Arrange
            Ranger ranger1 = new Ranger("MrRanger");
            string expected = "STR=1, DEX=7, INT=1";
            // Act
            string actual = $"STR={ranger1.Strength.CurrentAmount}, DEX={ranger1.Dexterity.CurrentAmount}, INT={ranger1.Intelligence.CurrentAmount}";
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Rogue_RogueStatsUponCreation_ShouldReturnSTR2DEX6INT1()
        {
            // Arrange
            Rogue rogue1 = new Rogue("MrsRogue");
            string expected = "STR=2, DEX=6, INT=1";
            // Act
            string actual = $"STR={rogue1.Strength.CurrentAmount}, DEX={rogue1.Dexterity.CurrentAmount}, INT={rogue1.Intelligence.CurrentAmount}";
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Warrior_WarriorStatsUponCreation_ShouldReturnSTR5DEX2INT1()
        {
            // Arrange
            Warrior warrior1 = new Warrior("MrsWarrior");
            string expected = "STR=5, DEX=2, INT=1";
            // Act
            string actual = $"STR={warrior1.Strength.CurrentAmount}, DEX={warrior1.Dexterity.CurrentAmount}, INT={warrior1.Intelligence.CurrentAmount}";
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Mage_MageStatsUponLevelUp_ShouldReturnSTR2DEX2INT13()
        {
            // Arrange
            Mage mage1 = new Mage("MrMage");
            string expected = "STR=2, DEX=2, INT=13";
            // Act
            mage1.LevelUp();
            string actual = $"STR={mage1.Strength.CurrentAmount}, DEX={mage1.Dexterity.CurrentAmount}, INT={mage1.Intelligence.CurrentAmount}";
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Ranger_RangerStatsUponLevelUp_ShouldReturnSTR2DEX12INT2()
        {
            // Arrange
            Ranger ranger1 = new Ranger("MrRanger");
            string expected = "STR=2, DEX=12, INT=2";
            // Act
            ranger1.LevelUp();
            string actual = $"STR={ranger1.Strength.CurrentAmount}, DEX={ranger1.Dexterity.CurrentAmount}, INT={ranger1.Intelligence.CurrentAmount}";
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Rogue_RogueStatsUponLevelUp_ShouldReturnSTR3DEX10INT2()
        {
            // Arrange
            Rogue rogue1 = new Rogue("MrsRogue");
            string expected = "STR=3, DEX=10, INT=2";
            // Act
            rogue1.LevelUp();
            string actual = $"STR={rogue1.Strength.CurrentAmount}, DEX={rogue1.Dexterity.CurrentAmount}, INT={rogue1.Intelligence.CurrentAmount}";
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Warrior_WarriorStatsUponLevelUp_ShouldReturnSTR8DEX4INT2()
        {
            // Arrange
            Warrior warrior1 = new Warrior("MrsWarrior");
            string expected = "STR=8, DEX=4, INT=2";
            // Act
            warrior1.LevelUp();
            string actual = $"STR={warrior1.Strength.CurrentAmount}, DEX={warrior1.Dexterity.CurrentAmount}, INT={warrior1.Intelligence.CurrentAmount}";
            // Assert
            Assert.Equal(expected, actual);
        }

    }
}
﻿using Xunit;
using RPGCharacters;

namespace RPGCharactersTests
{
    public class ItemTests
    {
        [Fact]
        public void EquipWeapon_Level1WarriorEquipsLevel2Axe_ShouldThrowInvalidWeaponException()
        {
            // Arrange
            Warrior warrior1 = new Warrior("Mr Warrior");
            Weapon testAxe = new Weapon("Mr Axe", 2, WeaponType.Axe);
            // Assert
            Assert.Throws<InvalidWeaponException>(() => warrior1.EquipWeapon(testAxe));
        }

        [Fact]
        public void EquipArmour_Level1WarriorEquipsLevel2Armour_ShouldThrowInvalidArmourException()
        {
            // Arrange
            Warrior warrior1 = new Warrior("Mr Warrior");
            Armour testPlateArmour = new Armour("Mr Plate", 2, ArmourType.Plate, SlotType.Body);
            // Assert
            Assert.Throws<InvalidArmourException>(() => warrior1.EquipArmour(testPlateArmour));
        }

        [Fact]
        public void EquipWeapon_WarriorEquipsBow_ShouldThrowInvalidWeaponException()
        {
            // Arrange
            Warrior warrior1 = new Warrior("Mr Warrior");
            Weapon testBow = new Weapon("Mr Bow", 1, WeaponType.Bow);
            // Assert
            Assert.Throws<InvalidWeaponException>(() => warrior1.EquipWeapon(testBow));
        }

        [Fact]
        public void EquipArmour_WarriorEquipsClothArmour_ShouldThrowInvalidArmourException()
        {
            // Arrange
            Warrior warrior1 = new Warrior("Mr Warrior");
            Armour testClothArmour = new Armour("Mr Cloth", 1, ArmourType.Cloth, SlotType.Body);
            // Assert
            Assert.Throws<InvalidArmourException>(() => warrior1.EquipArmour(testClothArmour));
        }

        [Fact]
        public void EquipWeapon_WarriorEquipsValidWeapon_ShouldReturnNewWeaponEquipped()
        {
            // Arrange
            Warrior warrior1 = new Warrior("Mr Warrior");
            Weapon testAxe = new Weapon("Mr Axe", 1, WeaponType.Axe);
            string expected = "New weapon equipped!";
            // Act
            string actual = warrior1.EquipWeapon(testAxe);
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void EquipArmour_WarriorEquipsValidArmour_ShouldReturnNewArmourEquipped()
        {
            // Arrange
            Warrior warrior1 = new Warrior("Mr Warrior");
            Armour testPlateArmour = new Armour("Mr Plate", 1, ArmourType.Plate, SlotType.Body);
            string expected = "New armour equipped!";
            // Act
            string actual = warrior1.EquipArmour(testPlateArmour);
            // Assert
            Assert.Equal(expected, actual);
        }


        [Fact]
        public void GetCharacterDamage_WarriorLevel1NoWeaponNoArmour_ShouldReturn105()
        {
            // Arrange
            Warrior warrior1 = new Warrior("Mr Warrior");
            double expected = 1.05; // 1*(1 + (5 / 100))
            // Act
            double actual = warrior1.GetCharacterDamage();
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetCharacterDamage_WarriorLevel1WithAxe_ShouldReturn8085()
        {
            // Arrange
            Warrior warrior1 = new Warrior("Mr Warrior");
            Weapon testAxe = new Weapon("Mr Axe", 1, WeaponType.Axe);
            warrior1.EquipWeapon(testAxe);
            double expected = 8.085; // (7 * 1.1) * (1 + (5 / 100))
            // Act
            double actual = warrior1.GetCharacterDamage();
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetCharacterDamage_WarriorLevel1WithAxeAndPlateArmour_ShouldReturn8162()
        {
            // Arrange
            Warrior warrior1 = new Warrior("Mr Warrior");
            Weapon testAxe = new Weapon("Mr Axe", 1, WeaponType.Axe);
            Armour testPlateArmour = new Armour("Mr Plate", 1, ArmourType.Plate, SlotType.Body);
            warrior1.EquipWeapon(testAxe);
            warrior1.EquipArmour(testPlateArmour);
            double expected = 8.162; //  (7 * 1.1) * (1 + ((5+1) / 100))
            // Act
            double actual = warrior1.GetCharacterDamage();
            // Assert
            Assert.Equal(expected, actual);
        }


    }
}

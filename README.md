# RPG Characters Assignment

This console app was written in C# and uses .NET 6.0. The app allows users to create Mage, Ranger, Rogue and Warrior characters, equip them with armour and weapons, get their damage scores and display their stats. In addition to the RPGCharacters project, the solution includes a RPGCharactersTests project, which contains xUnit tests that test the functionality of the characters and items (weapons and armour).

## Background

I created this C# console app as an assignment for the backend course of Noroff School of Technology and Digital Media. The course is part of the software development traineeship at Experis (Manpower Group). The goal of this assignment is to gain skills in object oriented programming, using the SOLID principles.

## Install

This application can be run in Visual Studio 2022.

## Usage

1. To create a Mage, Ranger, Rogue or Warrior character, create a new instance of the class and enter a name (type string) as parameter. For example: `Warrior warrior1 = new Warrior("WilliamTheWarrior");`
2. To create a weapon, create a new instance of the Weapon class and enter name, level and weapontype as parameters. For example: `Weapon testAxe = new Weapon("MyAxe", 1, WeaponType.Axe);`. Please note that weapon level cannot be higher than character level and different character types cannot use all types of weapons.
3. To create a piece of armour, create a new instance of the Armour class and enter name, level, armour type and slot type (Head, Body or Legs) as parameters. For example: `Armour testPlateArmour = new Armour("Mr Plate", 1, ArmourType.Plate, SlotType.Body);`. Please note that armour level cannot be higher than character level and different character types cannot use all types of armour.
4. To level up a character use: `warrior1.LevelUp()`
5. To get the character damage (calculated by character level plus any additional weapon/ armour) use: `Console.WriteLine(warrior1.GetCharacterDamage())`
6. To get the character stats use: `warrior1.DisplayStats()`

## License

MIT

Copyright (c) 2022 Dorine van Belzen

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
    internal class WeaponAttributes
    {

        // The damage and attack speed depend on weapon type, so instead of letting the user enter the stats,
        // the values are added from the Weapon Dictionary.
        public WeaponAttributes(WeaponType type)
        {
            Type = type;
            Damage = WeaponDictionary.weapons[Type][0];
            AttackSpeed = WeaponDictionary.weapons[Type][1];
        }

        public WeaponType Type { get; }
        public double Damage { get; }
        public double AttackSpeed { get; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
    internal class WeaponDictionary
    {
        public static Dictionary<WeaponType, double[]> weapons = new Dictionary<WeaponType, double[]>()
        {   
            // First int is Damage, second int is AttackSpeed (attacks per second)
            {WeaponType.Axe, new double[]{7, 1.1} },
            {WeaponType.Bow, new double[]{3, 2} },
            {WeaponType.Dagger, new double[]{3, 1.5} },
            {WeaponType.Hammer, new double[]{5, 1} },
            {WeaponType.Staff, new double[]{5, 1.2} },
            {WeaponType.Sword, new double[]{7, 1.1} },
            {WeaponType.Wand, new double[]{4, 1.5} }
        };
    }
}

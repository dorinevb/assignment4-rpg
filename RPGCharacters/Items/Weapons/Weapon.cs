﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RPGCharacters
{
    public class Weapon : Item
    {
        
        public Weapon(string name, int level, WeaponType type) : base(name, level)
        {
            Type = type;
            Damage = new WeaponAttributes(Type).Damage;
            AttackSpeed = new WeaponAttributes(Type).AttackSpeed;
            DPS = Damage * AttackSpeed;
            ItemSlot = SlotType.Weapon;
        }

        public WeaponType Type { get; }
        public double Damage { get; }
        public double AttackSpeed { get; }
        public double DPS { get; } // DPS = damage per second

    }
}
﻿using System;

namespace RPGCharacters
{
    public class InvalidWeaponException : Exception
    {
        public InvalidWeaponException()
        {
        }

        public InvalidWeaponException(string weaponName)
            : base(String.Format("Invalid weapon chosen: {0}", weaponName))
        {
        }
    }
}

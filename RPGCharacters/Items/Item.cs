﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RPGCharacters
{
    // Item is an abstract class, because only the subclasses need to have instances.
    public abstract class Item
    {
        protected Item(string name, int level)
        {
            ItemName = name;
            ItemLevel = level;
        }

        public string ItemName { get; set; }
        public int ItemLevel { get; set; }
        public SlotType ItemSlot { get; set; }
    }
}
﻿using System;

namespace RPGCharacters
{
    public class InvalidArmourException : Exception
    {
        public InvalidArmourException()
        {
        }

        public InvalidArmourException(string armourName)
            : base(String.Format("Invalid armour chosen: {0}", armourName))
        {
        }
    }
}


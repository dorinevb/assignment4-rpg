﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
    internal class ArmourDictionary
    {
        public static Dictionary<ArmourType, int> armour = new Dictionary<ArmourType, int>()
        {   
            // Key is armour type, value is damage
            {ArmourType.Cloth, 1 },
            {ArmourType.Leather, 1 },
            {ArmourType.Mail, 1 },
            {ArmourType.Plate, 1 }
        };
    }
}

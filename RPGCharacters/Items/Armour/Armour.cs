﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RPGCharacters
{
    public class Armour : Item
    {
        // Armour can be used for the head, body or legs, therefore user enters a 'SlotType' as parameter upon creating an armour instance.
        public Armour(string name, int level, ArmourType type, SlotType armourSlot) : base(name, level)
        {
            Type = type;
            ItemSlot = armourSlot;
            Strength = new PrimaryAttribute(AttributeType.Strength, 1, 1);
            Dexterity = new PrimaryAttribute(AttributeType.Dexterity, 1, 1);
            Intelligence = new PrimaryAttribute(AttributeType.Intelligence, 1, 1);
        }

        public Armour(string name, int level, PrimaryAttribute strength, PrimaryAttribute dexterity, PrimaryAttribute intelligence) : base(name, level)
        {
            Strength = strength;
            Dexterity = dexterity;
            Intelligence = intelligence;
        }

        public ArmourType Type { get; }

        // Using Primary Attributes here because of assignment instruction,
        // if it were up to me I would have made a single ArmourAttribute 'Protection'
        public PrimaryAttribute Strength { get; }
        public PrimaryAttribute Dexterity { get; }
        public PrimaryAttribute Intelligence { get; }
    }
}
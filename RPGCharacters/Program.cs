﻿using System;

namespace RPGCharacters
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Warrior warrior1 = new Warrior("Hank");
            warrior1.DisplayStats();
            Console.ReadLine();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RPGCharacters
{
    public class PrimaryAttribute
    {
        
        private AttributeType Type;
        
        public PrimaryAttribute(AttributeType type, int currentAmount, int increaseAmount)
        {
            Type = type;
            CurrentAmount = currentAmount;
            IncreaseAmount = increaseAmount;
        }

        // Properties CurrentAmount and IncreaseAmount are set in the different Character classes,
        // e.g. for a Mage the CurrentAmount of Intelligence is initially set to 8 and
        // the IncreaseAmount (amount by which Intelligence is increase on level up) is set to 5.
        public int CurrentAmount { get; set; }
        public int IncreaseAmount { get; set; }

        /// <summary>
        /// Increases a character's primary attribute upon leveling up, by adding the IncreaseAmount to the CurrentAmount.
        /// </summary>
        public virtual void Add()
        {
            CurrentAmount += IncreaseAmount;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RPGCharacters
{
    // Character is an abstract class, because only the subclasses need to have instances.
    public abstract class Character
    {
        // Use protected modifier because only subclasses need to call the constructor
        protected Character(string name)
        {
            Name = name;
            Level = 1;
            AvailableWeapons = new List<WeaponType?>();
            AvailableArmour = new List<ArmourType?>();
        }

        public string Name { get; set; }
        public int Level { get; set; }

        // List of weapons that the character can use
        public List<WeaponType?> AvailableWeapons { get; set; }
        // List of armour that the character can use
        public List<ArmourType?> AvailableArmour { get; set; }

        // Dictionary where armour and weapons can be stored in the different slots.
        public Dictionary<SlotType, Item?> ItemSlots = new Dictionary<SlotType, Item?>()
        {
            { SlotType.Head, null },
            { SlotType.Body, null },
            { SlotType.Legs, null },
            { SlotType.Weapon, null }
        };           

        /// <summary>
        /// Levels up a character. The method is abstract because different types of characters level up in different ways.
        /// </summary>
        public abstract void LevelUp();
        
        /// <summary>
        /// Equips a character with a new weapon. Checks if weapon is valid, if so returns success message.
        /// </summary>
        /// <param name="newWeapon">Weapon object</param>
        /// <returns>A string with success message.</returns>
        /// <exception cref="InvalidWeaponException">Thrown if weapon level is too high or type of weapon is not available to character.</exception>
        public virtual string EquipWeapon(Weapon newWeapon)
        {
            if (newWeapon.ItemLevel > Level || !AvailableWeapons.Contains(newWeapon.Type))
            {
                throw new InvalidWeaponException(newWeapon.ItemName);
            }
            ItemSlots[newWeapon.ItemSlot] = newWeapon;
            return $"New weapon equipped!";
        }

        /// <summary>
        /// Equips a character with new armour. Checks if armour is valid, if so returns success message.
        /// </summary>
        /// <param name="newArmour">Armour object</param>
        /// <returns>A string with success message.</returns>
        /// <exception cref="InvalidArmourException">Thrown if weapon level is too high or type of weapon is not available to character.</exception>
        public virtual string EquipArmour(Armour newArmour)
        {
            if (newArmour.ItemLevel > Level || !AvailableArmour.Contains(newArmour.Type))
            {
                throw new InvalidArmourException();
            }
            ItemSlots[newArmour.ItemSlot] = newArmour;
            return $"New armour equipped!";
        }

        /// <summary>
        /// Returns Total Attribute: the damage of a character based on character level and armour.
        /// Depending on character type damage is calculated with Strength, Dexterity or Intelligence.
        /// </summary>
        /// <returns>An integer representing the Total Attribute.</returns>
        public abstract int GetTotalAttribute();


        /// <summary>
        /// Returns Character Damage: Weapon DamagePerSecond * (1 + TotalPrimaryAttribute/100).
        /// </summary>
        /// <returns>A double representing the Character Damage.</returns>
        public virtual double GetCharacterDamage()
        {
            double totalAttribute = Convert.ToDouble(GetTotalAttribute());
            double weaponDPS = 1.0;
            if (ItemSlots[SlotType.Weapon] != null)
            {
                weaponDPS = Convert.ToDouble(((Weapon)ItemSlots[SlotType.Weapon]!).DPS);
            }
            double totalDamage = weaponDPS * (1 + totalAttribute / 100);
            return totalDamage;
        }

    }

}

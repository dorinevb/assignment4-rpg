﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RPGCharacters
{
    public class Warrior : Character, IDisplay
    {
        // Set Strength, Dexterity and Intelligence in Warrior Class instead of Character class,
        // because in future there may be different types of characters with different attributes.
        // E.g. maybe there will be a druid type which uses Wisdom instead of Intelligence.
        public Warrior(string name) : base(name)
        {
            Strength = new PrimaryAttribute(AttributeType.Strength, 5, 3);
            Dexterity = new PrimaryAttribute(AttributeType.Dexterity, 2, 2);
            Intelligence = new PrimaryAttribute(AttributeType.Intelligence, 1, 1);
            AvailableWeapons = new List<WeaponType?>() { WeaponType.Axe, WeaponType.Hammer, WeaponType.Sword };
            AvailableArmour = new List<ArmourType?>() { ArmourType.Mail, ArmourType.Plate };
        }

        public Warrior(string name, PrimaryAttribute strength, PrimaryAttribute dexterity, PrimaryAttribute intelligence) : base(name)
        {
            Strength = strength;
            Dexterity = dexterity;
            Intelligence = intelligence;
        }

        public PrimaryAttribute Strength { get; }
        public PrimaryAttribute Dexterity { get; }
        public PrimaryAttribute Intelligence { get; }

        /// <summary>
        /// Character goes up 1 level and primary attributes increase accordingly.
        /// </summary>
        public override void LevelUp()
        {
            Level = Level + 1;
            Strength.Add();
            Dexterity.Add();
            Intelligence.Add();
        }

        /// <summary>
        /// Calculates 'total attribute', which is the damage based on character level and armour.
        /// For a warrior the strength attribute is used to calculate damage.
        /// </summary>
        /// <returns>An integer representing the total attribute.</returns>
        public override int GetTotalAttribute()
        {
            int totalAttribute = Strength.CurrentAmount;
            if(ItemSlots[SlotType.Head] != null) totalAttribute += ((Armour)ItemSlots[SlotType.Head]!).Strength.CurrentAmount;
            if (ItemSlots[SlotType.Body] != null) totalAttribute += ((Armour)ItemSlots[SlotType.Body]!).Strength.CurrentAmount;
            if (ItemSlots[SlotType.Legs] != null) totalAttribute += ((Armour)ItemSlots[SlotType.Legs]!).Strength.CurrentAmount;
            return totalAttribute;
        }

        public void DisplayStats()
        {
            StringBuilder sb = new StringBuilder("Character Stats\n\n");
            sb.AppendLine($"Name: {this.Name}");
            sb.AppendLine($"Warrior of level: {this.Level}");
            sb.AppendLine($"Strength: {this.GetTotalAttribute()}");
            sb.AppendLine($"Dexterity: {this.Dexterity.CurrentAmount}");
            sb.AppendLine($"Intelligence: {this.Intelligence.CurrentAmount}");
            sb.AppendLine($"Damage: {this.GetCharacterDamage()}");
            Console.WriteLine(sb);
        }
    }
}



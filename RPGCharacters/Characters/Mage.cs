﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RPGCharacters
{
    public class Mage : Character, IDisplay
    {
        // Set Strength, Dexterity and Intelligence in Mage Class instead of Character class,
        // because in future there may be different types of characters with different attributes.
        // E.g. maybe there will be a druid type which uses Wisdom instead of Intelligence.
        public Mage(string name) : base(name)
        {
            Strength = new PrimaryAttribute(AttributeType.Strength, 1, 1);
            Dexterity = new PrimaryAttribute(AttributeType.Dexterity, 1, 1);
            Intelligence = new PrimaryAttribute(AttributeType.Intelligence, 8, 5);
            AvailableWeapons = new List<WeaponType?>() { WeaponType.Staff, WeaponType.Wand };
            AvailableArmour = new List<ArmourType?>() { ArmourType.Cloth };
        }

        public Mage(string name, PrimaryAttribute strength, PrimaryAttribute dexterity, PrimaryAttribute intelligence) : base(name)
        {
            Strength = strength;
            Dexterity = dexterity;
            Intelligence = intelligence;
        }

        public PrimaryAttribute Strength { get; }
        public PrimaryAttribute Dexterity { get; }
        public PrimaryAttribute Intelligence { get; }

        /// <summary>
        /// Character goes up 1 level and primary attributes increase accordingly.
        /// </summary>
        public override void LevelUp()
        {
            Level = Level + 1;
            Strength.Add();           
            Dexterity.Add();          
            Intelligence.Add();
        }
        
        /// <summary>
        /// Calculates 'total attribute', which is the damage based on character level and armour.
        /// For a mage the Intelligence attribute is used to calculate damage.
        /// </summary>
        /// <returns>An integer representing the total attribute.</returns>
        public override int GetTotalAttribute()
        {
            int totalAttribute = Intelligence.CurrentAmount;
            if (ItemSlots[SlotType.Head] != null) totalAttribute += ((Armour)ItemSlots[SlotType.Head]!).Intelligence.CurrentAmount;
            if (ItemSlots[SlotType.Body] != null) totalAttribute += ((Armour)ItemSlots[SlotType.Body]!).Intelligence.CurrentAmount;
            if (ItemSlots[SlotType.Legs] != null) totalAttribute += ((Armour)ItemSlots[SlotType.Legs]!).Intelligence.CurrentAmount;
            return totalAttribute;
        }

        public void DisplayStats()
        {
            StringBuilder sb = new StringBuilder("Character Stats\n\n");
            sb.AppendLine($"Name: {this.Name}");
            sb.AppendLine($"Mage of level: {this.Level}");
            sb.AppendLine($"Strength: {this.Strength.CurrentAmount}");
            sb.AppendLine($"Dexterity: {this.Dexterity.CurrentAmount}");
            sb.AppendLine($"Intelligence: {this.GetTotalAttribute()}");
            sb.AppendLine($"Damage: {this.GetCharacterDamage()}");
            Console.WriteLine(sb);
        }
    }
}


